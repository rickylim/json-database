# JSON  Database

## About

A multi-thread client-server application that allows multiple clients to store data in JSON format on the server.

## Learning outcomes

- Thread API and multi-thread management
- Working with JSON
- Socket API

## Build

```bash
./gradlew clean jar 
```

The `jar` is available `build/libs/json-database-1.0.jar`

## Demo 

```bash

# Start the server
$ java -cp build/libs/json-database-1.0.jar server.Main

# Start the client in another terminal
$ java -cp build/libs/json-database-1.0.jar client.Main -t set -k 1 -v "Hello world!"
Client data directory: C:\Users\901754\code\bitbucket\rickylim\java-developer-track\json-database\src\server\data
Client Started
Sent: {"type":"set","key":"1","value":"Hello world!"}
Received: {"response":"OK"}

$ java -cp build/libs/json-database-1.0.jar client.Main -t get -k 1
Client data directory: C:\Users\901754\code\bitbucket\rickylim\java-developer-track\json-database\src\server\data
Client Started
Sent: {"type":"get","key":"1"}
Received: {"response":"OK","value":"Hello world!"}

# Set with nested keys
$ java -cp build/libs/json-database-1.0.jar client.Main -t set -k "["person", "firstname"]" -v "Ricky"
Client data directory: C:\Users\901754\code\bitbucket\rickylim\java-developer-track\json-database\src\server\data
Client Started
Sent: {"type":"set","key":"[person, firstname]","value":"Ricky"}
Received: {"response":"OK"}

# Get with nested keys
java -cp build/libs/json-database-1.0.jar client.Main -t get -k "["person", "firstname"]"
Client data directory: C:\Users\901754\code\bitbucket\rickylim\java-developer-track\json-database\src\server\data
Client Started
Sent: {"type":"get","key":"[person, firstname]"}
Received: {"response":"OK","value":"Ricky"}

# Delete operations
$  java -cp build/libs/json-database-1.0.jar client.Main -t delete -k "["person", "surname"]"
Client data directory: C:\Users\901754\code\bitbucket\rickylim\java-developer-track\json-database\src\server\data
Client Started
Sent: {"type":"delete","key":"[person, surname]"}
Received: {"response":"OK"}

$  java -cp build/libs/json-database-1.0.jar client.Main -t get -k "["person", "surname"]"
Client data directory: C:\Users\901754\code\bitbucket\rickylim\java-developer-track\json-database\src\server\data
Client Started
Sent: {"type":"get","key":"[person, surname]"}
Received: {"response":"OK"}
```