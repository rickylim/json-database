package client;

import com.beust.jcommander.Parameter;
import com.google.gson.annotations.Expose;

public class CommandLineArgs {
    @Expose
    @Parameter(
            names = {"-t", "--type"},
            description = "Type of the request",
            order = 0
    )
    public String type;

    @Expose
    @Parameter(
            names = {"-k", "--key"},
            description = "key record",
            order = 1
    )
    public String key;

    @Expose
    @Parameter(
            names = {"-v", "--value"},
            description = "Value to store",
            order = 2
    )
    public String value;

    @Parameter(
            names = {"-in", "--input-file"},
            description = "Input file containing the request in JSON",
            order = 3)
    public String filename;
}