package client;


import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

public class Main {
    public static void main(String[] args) {
        CommandLineArgs cla = new CommandLineArgs();
        JCommander jCommander = new JCommander(cla);
        try {
            jCommander.parse(args);
            Client.start(cla);
        } catch (ParameterException e) {
            System.out.println("Wrong parameter: " + e.getMessage());
            e.usage();
        }
    }
}