package client;

import com.google.gson.Gson;
import server.Server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Client {

    private static final Path DATA_DIR_PATH = Paths.get(
            "src" + File.separator +
                    "client" + File.separator +
                    "data").toAbsolutePath();

    private static final String ADDRESS = "localhost";
    private static final int PORT = 9292;

    public static void start(CommandLineArgs cla) {
        createDataDir();
        System.out.println("Client Started");
        try (
                Socket socket = new Socket(InetAddress.getByName(ADDRESS), PORT);
                DataInputStream input = new DataInputStream(socket.getInputStream());
                DataOutputStream output = new DataOutputStream(socket.getOutputStream())
        ) {
            String request = cla.filename != null
                    ? Files.readString(DATA_DIR_PATH.resolve(cla.filename))
                    : new Gson().toJson(cla);

            output.writeUTF(request);
            System.out.println("Sent: " + request);
            System.out.println("Received: " + input.readUTF());
        } catch (NoSuchFileException e) {
            System.out.println("Cannot read file: " + e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(
                    String.format("IO exception occurs with command line args: %s. Error: %s", cla, e.getMessage())
            );
        }
    }

    private static void createDataDir() {
        File dir = new File(Client.DATA_DIR_PATH.toString());
        boolean createdDir = dir.mkdirs();
        if (createdDir){
            System.out.printf("Client data directory: %s, created%n", Server.DATA_DIR_PATH);
        } else {
            System.out.printf("Client data directory: %s%n", Server.DATA_DIR_PATH);
        }
    }
}