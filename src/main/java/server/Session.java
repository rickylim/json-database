package server;

import com.google.gson.Gson;
import server.commands.CommandExecutor;
import server.commands.DeleteCommand;
import server.commands.GetCommand;
import server.commands.SetCommand;
import server.wrappers.Request;
import server.wrappers.Response;

import java.io.*;
import java.net.Socket;

public class Session implements Runnable {
    private final static String GET = "get";
    private final static String SET = "set";
    private final static String DELETE = "delete";
    private final static String EXIT = "exit";

    private final CommandExecutor executor = new CommandExecutor();
    private final Socket socket;
    private final DataInputStream input;
    private final DataOutputStream output;
    private final Request request;
    private final Response response;

    public Session(Socket socket) throws IOException {
        this.socket = socket;
        this.input = new DataInputStream(socket.getInputStream());
        this.output = new DataOutputStream(socket.getOutputStream());

        this.request = new Gson().fromJson(input.readUTF(), Request.class);
        if (request.getType().equals("exit")) {
            Server.exit();
        }

        this.response = new Response();
    }


    @Override
    public void run() {
        try (socket; input; output) {
            try {
                switch (request.getType()) {
                    case GET:
                        GetCommand getCmd = new GetCommand(request.getKey());
                        executor.executeCommand(getCmd);
                        response.setValue(getCmd.getResult());
                        break;
                    case SET:
                        SetCommand setCmd = new SetCommand(
                                request.getKey(),
                                request.getValue());
                        executor.executeCommand(setCmd);
                        break;
                    case DELETE:
                        DeleteCommand deleteCmd = new DeleteCommand(
                                request.getKey());
                        executor.executeCommand(deleteCmd);
                        break;
                    case EXIT:
                        break;
                    default:
                        throw new IllegalArgumentException(
                                String.format("Request type: %s is unknown", request.getType())
                        );
                }

                response.setResponse(Response.STATUS_OK);

            } catch (Exception e) {
                response.setResponse(Response.STATUS_ERROR);
                response.setReason(e.getMessage());
            } finally {
                output.writeUTF(new Gson().toJson(response));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}