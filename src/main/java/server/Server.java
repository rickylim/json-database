package server;

import server.database.Database;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;


public class Server {

    public static final Path DATA_DIR_PATH = Paths.get(
            "src" + File.separator +
                    "server" + File.separator +
                    "data").toAbsolutePath();

    private static final String ADDRESS = "localhost";

    private static final int PORT = 9292;
    private static final int BACKLOG = 50;
    private static final int THREAD_COUNT;

    private static final ExecutorService EXECUTOR_SERVICE;
    private static final AtomicBoolean isAlive = new AtomicBoolean(true);

    static {
        THREAD_COUNT = Runtime.getRuntime().availableProcessors();
        EXECUTOR_SERVICE = Executors.newFixedThreadPool(THREAD_COUNT);
    }

    public static void exit() {
        isAlive.set(false);
    }

    public static void start() {
        createDataDir();
        System.out.println("Server started!");
        try (ServerSocket socket = new ServerSocket(PORT, BACKLOG, InetAddress.getByName(ADDRESS))) {
            Database.getInstance().init();

            while (isAlive.get()) {
                Session session = new Session(socket.accept());
                EXECUTOR_SERVICE.submit(session);
            }

            EXECUTOR_SERVICE.shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createDataDir() {
        File dir = new File(Server.DATA_DIR_PATH.toString());
        boolean createdDir = dir.mkdirs();
        if (createdDir) {
            System.out.printf("Server directory: %s, created%n", Server.DATA_DIR_PATH);
        } else {
            System.out.printf("Server directory: %s%n", Server.DATA_DIR_PATH);
        }
    }
}