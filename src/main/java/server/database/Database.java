package server.database;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import server.Server;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.UnaryOperator;

public class Database {
    private static final String FILENAME = "db.json";
    private static final Path DB_PATH = Server.DATA_DIR_PATH.resolve(FILENAME);
    private static final Gson GSON = new Gson();
    private static final Type LIST_TYPE = new TypeToken<List<String>>() {
    }.getType();
    private static final UnaryOperator<String> UNSUPPORTED_KEY_ERROR_MESSAGE = k -> String.format(
            "Supported key is json primitive or array. But provided key: %s", k);
    private volatile static Database uniqueInstance;
    private final Lock readLock;
    private final Lock writeLock;
    private JsonObject jsonData;

    {
        ReadWriteLock lock = new ReentrantReadWriteLock();
        writeLock = lock.writeLock();
        readLock = lock.readLock();
    }

    private Database() {
    }

    public static Database getInstance() {
        if (uniqueInstance == null) {
            synchronized (Database.class) {
                if (uniqueInstance == null) {
                    uniqueInstance = new Database();
                }
            }
        }
        return uniqueInstance;
    }

    private static JsonObject createNestedElement(List<String> keys, JsonElement value, JsonObject data) {
        String key = keys.get(0);

        if (isNested(keys)) {
            var nextKeys = keys.subList(1, keys.size());
            JsonObject nextObject = getOrCreateObject(key, data);
            data.add(key, createNestedElement(nextKeys, value, nextObject));
        } else {
            data.add(key, value);
        }

        return data;
    }

    private static JsonObject getOrCreateObject(String key, JsonObject data) {
        return data.get(key) != null ? data.get(key).getAsJsonObject() : new JsonObject();
    }

    private static void deleteNestedElement(List<String> keys, JsonObject data) {
        String key = keys.get(0);
        if (isNested(keys)) {
            JsonElement nextElement = data.get(key);
            if (nextElement == null) {
                return;
            }
            var nextKeys = keys.subList(1, keys.size());
            var nextData = nextElement.getAsJsonObject();
            deleteNestedElement(nextKeys, nextData);
        } else {
            data.getAsJsonObject().remove(key);
        }
    }

    private static JsonElement findNestedElement(List<String> keys, JsonObject data) {
        String key = keys.get(0);
        if (isNested(keys)) {
            JsonElement nextElement = data.get(key);
            if (nextElement == null) {
                return null;
            }
            var nextKeys = keys.subList(1, keys.size());
            var nextData = nextElement.getAsJsonObject();
            return findNestedElement(nextKeys, nextData);
        } else {
            return data.getAsJsonObject().get(key);
        }
    }

    private static boolean isNested(List<String> keys) {
        return keys.size() > 1;
    }

    public void init() throws IOException {
        if (Files.exists(DB_PATH)) {
            String content = Files.readString(DB_PATH);
            jsonData = new Gson().fromJson(content, JsonObject.class);
            if (jsonData == null) {
                jsonData = new JsonObject();
            }
        } else {
            Files.createFile(DB_PATH);
            jsonData = new JsonObject();
            writeToDatabase();
        }
    }

    public void set(JsonElement key, JsonElement value) {
        try {
            writeLock.lock();

            if (key.isJsonPrimitive()) {
                jsonData.add(key.getAsString(), value);
            } else if (key.isJsonArray()) {
                List<String> keys = GSON.fromJson(key, LIST_TYPE);
                createNestedElement(keys, value, jsonData);
            } else {
                throw new RuntimeException(UNSUPPORTED_KEY_ERROR_MESSAGE.apply(key.toString()));
            }

            writeToDatabase();
        } finally {
            writeLock.unlock();
        }
    }

    public JsonElement get(JsonElement key) {
        try {
            readLock.lock();

            if (key.isJsonPrimitive()) {
                return jsonData.get(key.getAsString());
            }

            if (key.isJsonArray()) {
                List<String> keys = GSON.fromJson(key, LIST_TYPE);
                return findNestedElement(keys, jsonData);
            }

            throw new RuntimeException(UNSUPPORTED_KEY_ERROR_MESSAGE.apply(key.toString()));

        } finally {
            readLock.unlock();
        }
    }

    public void delete(JsonElement key) {
        try {
            writeLock.lock();

            if (key.isJsonPrimitive() && jsonData.has(key.getAsString())) {
                jsonData.remove(key.getAsString());
            } else if (key.isJsonArray()) {
                List<String> keys = GSON.fromJson(key, LIST_TYPE);
                deleteNestedElement(keys, jsonData);
            } else {
                throw new RuntimeException(UNSUPPORTED_KEY_ERROR_MESSAGE.apply(key.toString()));
            }

            writeToDatabase();

        } finally {
            writeLock.unlock();
        }
    }

    private void writeToDatabase() {
        try (FileWriter writer = new FileWriter(DB_PATH.toString())) {
            writer.write(new Gson().toJson(jsonData));
        } catch (IOException e) {
            throw new RuntimeException(
                    String.format("IO exception writing to %s. Error: %s", DB_PATH, e.getMessage())
            );
        }
    }

}