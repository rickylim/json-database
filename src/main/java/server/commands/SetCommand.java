package server.commands;

import com.google.gson.JsonElement;
import server.database.Database;

public class SetCommand implements Command {
    private final JsonElement key;
    private final JsonElement value;

    public SetCommand(JsonElement key, JsonElement value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public void execute() {
        Database.getInstance().set(key, value);
    }
}